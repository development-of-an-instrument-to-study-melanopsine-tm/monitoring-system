Version 4
SHEET 1 1256 680
WIRE 896 -208 640 -208
WIRE 1040 -208 960 -208
WIRE 0 -160 0 -208
WIRE 0 -64 0 -80
WIRE 288 -64 288 -96
WIRE 384 -64 288 -64
WIRE 960 -32 960 -48
WIRE 1088 -32 960 -32
WIRE 288 0 288 -64
WIRE -128 16 -512 16
WIRE 0 16 -48 16
WIRE 256 16 192 16
WIRE 960 16 960 -32
WIRE 432 32 320 32
WIRE 496 32 432 32
WIRE 640 32 640 -208
WIRE 640 32 576 32
WIRE 656 32 640 32
WIRE 832 32 816 32
WIRE 928 32 912 32
WIRE 0 48 0 16
WIRE 256 48 0 48
WIRE 1040 48 1040 -208
WIRE 1040 48 992 48
WIRE 1168 48 1040 48
WIRE 832 64 832 32
WIRE 928 64 832 64
WIRE 288 80 288 64
WIRE 384 80 384 0
WIRE 384 80 288 80
WIRE 1088 80 1088 32
WIRE 1088 80 960 80
WIRE 288 96 288 80
WIRE 832 96 832 64
WIRE 960 96 960 80
WIRE -512 144 -512 16
WIRE 0 144 0 48
WIRE 912 160 912 32
WIRE 1040 160 1040 48
WIRE 1040 160 912 160
WIRE 192 176 192 16
WIRE 288 176 192 176
WIRE 336 176 288 176
WIRE 432 176 432 32
WIRE 432 176 416 176
WIRE 832 176 832 160
WIRE -400 272 -400 224
WIRE 0 272 0 208
WIRE 288 288 288 256
WIRE 832 304 832 240
FLAG 0 272 0
FLAG 0 -208 +vcc
FLAG 0 -64 0
FLAG 288 -96 +vcc
FLAG 288 288 0
FLAG -400 272 0
FLAG 960 -48 +vcc
FLAG 832 304 0
FLAG -512 224 0
FLAG 288 96 0
FLAG 960 96 0
SYMBOL Misc\\signal -400 128 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value PULSE(0 2.86 0 0 0 192u 384u 250)
SYMBOL res -32 0 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 10k
SYMATTR SpiceLine tol=1 pwr=0.6
SYMBOL res 592 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 10k
SYMATTR SpiceLine tol=1 pwr=0.6
SYMBOL voltage 0 -176 R0
WINDOW 123 0 0 Left 0
WINDOW 39 24 44 Left 2
SYMATTR SpiceLine Rser=1M Cpar=10n
SYMATTR InstName V2
SYMATTR Value 5
SYMBOL res 304 272 R180
WINDOW 0 36 76 Left 2
WINDOW 3 36 40 Left 2
SYMATTR InstName R3
SYMATTR Value 10k
SYMATTR SpiceLine tol=1 pwr=0.6
SYMBOL res 432 160 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 1k
SYMATTR SpiceLine tol=5 pwr=1
SYMBOL res 752 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R5
SYMATTR Value 5.1k
SYMATTR SpiceLine tol=1 pwr=0.6
SYMBOL res 832 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R6
SYMATTR Value 10k
SYMATTR SpiceLine tol=1 pwr=0.6
SYMBOL cap 816 176 R0
SYMATTR InstName C3
SYMATTR Value 0.22�
SYMATTR SpiceLine V=50 Irms=0 Rser=0 Lser=0 mfg="Murata" pn="GCM21BR71H224KA37" type="X7R"
SYMBOL voltage -512 128 R0
WINDOW 123 24 124 Left 2
WINDOW 39 0 0 Left 0
SYMATTR Value2 AC 3
SYMATTR InstName V3
SYMATTR Value ""
SYMBOL OpAmps\\LT1013 288 -32 R0
WINDOW 3 -39 158 Left 2
SYMATTR InstName U1
SYMBOL OpAmps\\LT1013 960 -16 R0
WINDOW 3 -33 142 Left 2
SYMATTR InstName U2
SYMBOL cap 368 -64 R0
SYMATTR InstName C4
SYMATTR Value 0.01�
SYMATTR SpiceLine V=6.3 Irms=0 Rser=0 Lser=0 mfg="Murata" pn="GRM033R70J103KA01" type="X7R"
SYMBOL cap 1072 -32 R0
SYMATTR InstName C5
SYMATTR Value 0.01�
SYMATTR SpiceLine V=6.3 Irms=0 Rser=0 Lser=0 mfg="Murata" pn="GRM033R70J103KA01" type="X7R"
SYMBOL cap 816 96 R0
SYMATTR InstName C7
SYMATTR Value 0.22�
SYMATTR SpiceLine V=50 Irms=0 Rser=0 Lser=0 mfg="Murata" pn="GCM21BR71H224KA37" type="X7R"
SYMBOL cap 960 -224 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C9
SYMATTR Value 0.47�
SYMATTR SpiceLine V=50 mfg="Murata" pn="GCM21BR71H474KA55" type="X7R"
SYMBOL cap -16 144 R0
SYMATTR InstName C1
SYMATTR Value 0.22�
SYMATTR SpiceLine V=50 Irms=0 Rser=0 Lser=0 mfg="Murata" pn="GCM21BR71H224KA37" type="X7R"
TEXT -496 -128 Left 2 !.ac dec 100 1 10k
TEXT -560 -176 Left 2 ;For frequency analysis: .ac dec 100 1 10k
TEXT -560 -216 Left 2 ;For time analysis: .tran 80m
