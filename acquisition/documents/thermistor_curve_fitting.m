x = [0.6, 1,  2.1,  3.4, 3.95]
y = [0,   15,  45,   80, 100]
n = 3
p = polyfit(x, y, n)

xx=0:0.1:100;
f = polyval(p,xx);
plot(x,y,'o',xx,f,'-')
axis([0, 4, 0, 100])
grid 'on'