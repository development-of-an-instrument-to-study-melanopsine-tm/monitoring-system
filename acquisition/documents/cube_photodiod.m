dc = [1, 10, 50, 100] # duty cycle percents
mv = [330, 420, 505, 542] # measured tension (cube coverd)

#pc = [0.1:0.1:100];
mvv = [300:1:550]
#logfit = 45 * log(pc) + 335;
expfit = e.^(mvv/45 - 335/45)

plot(mv, dc, 'o-', mvv, expfit, 'r-')
#plot(dc, mv, 'o-', pc, logfit, 'rx-')

#axis([0, 1, 300, 550])
grid 'on'
xlabel 'ADC input [mV]'
ylabel 'LED brightness [%]'

