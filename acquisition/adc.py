#!/usr/local/bin/python
"""
2020, Vincent Conus

Main Python script used to run the monitoring system.

This script can acquire the ADC data through a I2C communication,
save the raw measurement in a CSV file and display
statistics on the values on a remote or local dashboard.
The later are just sent using UDP packages.

The code is formatted following the Flake8 guideline.
"""
import warnings
import socket
from pathlib import Path
import queue
import sys
import time
import subprocess
import urllib.request
import json
import yaml
import smbus2 as smbus


# Here we load all the configuration values
# for the program from the confi YAML file
# and store them in a CONFIG dictionary
try:
    CONFIG = yaml.safe_load(open("config.yml"))
except OSError as error:
    print("Errno " +
          str(error.errno) +
          ": could not load or open config.yml file")
    sys.exit(0)


class PiHatADC():
    """
    Contains all the methods to read data from the ADC shield
    """
    bus = None

    def __init__(self,
                 bus_num=CONFIG['I2C']['BUS'],
                 addr=CONFIG['I2C']['ADC_DEFAULT_IIC_ADDR'],
                 blocksize=CONFIG['I2C']['BLOCKSIZE'],
                 channum=CONFIG['I2C']['ADC_CHAN_NUM']):

        if not PiHatADC.bus:
            PiHatADC.bus = smbus.SMBus(bus_num)
        self.addr = addr
        self.blocksize = blocksize
        self.channum = channum

    def read_channel(self, channel, register):
        """
        Get the data for all 8 channel for the requried register
        when the channel parameter is None.
        Else, get the  12-bits data for the given channel
        (from channel 0 to channel 7).

        The register value is given by the other method
        that will call this one for either the raw values,
        the milivolt measure or the rasio (0.1%).

        Since it is a 12-bits system, the values are
        from 0 to 4095 and the block that need to be
        read is two (2) bytes long.
        """
        # We read all the channels (no channel specified)
        # and store and return the values in an array
        if channel is None:
            array = []
            for i in range(self.channum):
                try:
                    data = self.bus.read_i2c_block_data(self.addr,
                                                        register
                                                        + i,
                                                        self.blocksize)
                # Try and handle properly the I2C error.
                # i.e. if there is no shield mounted.
                except OSError as error:
                    print("Errno " +
                          str(error.errno) +
                          ": could not use the I2C. " +
                          "The program will restart in 2 seconds.")
                    time.sleep(2)
                    sys.exit(0)

                # Read the two bytes of data and append them to the array
                val = data[1] << 8 | data[0]
                array.append(val)
            return array

        # Else, we read the ADC onli for the asked channel
        try:
            data = self.bus.read_i2c_block_data(self.addr,
                                                register
                                                + channel,
                                                self.blocksize)
        # Try and handle properly the I2C error.
        # i.e. if there is no shield mounted.
        except OSError as error:
            print("Errno " +
                  str(error.errno) +
                  ": could not use the I2C. " +
                  "The program will restart in 2 seconds.")
            time.sleep(2)
            sys.exit(0)

        # Read the two bytes of data and return that.
        val = data[1] << 8 | data[0]
        return val

    def get_adc_raw_data(self, channel=None):
        """
        Use the read_channel methode to read the raw data.
        """
        return self.read_channel(channel, CONFIG['I2C']['REG_RAW_START'])

    def get_vol_milli_data(self, channel=None):
        """
        Use the read_channel methode to read the milivolt data.
        """
        return self.read_channel(channel, CONFIG['I2C']['REG_VOL_START'])

    def get_ratio_0_1_data(self, channel=None):
        """
        Use the read_channel methode to read the ratio data.
        The value is a ratio in 0.1%
        """
        return self.read_channel(channel, CONFIG['I2C']['REG_RTO_START'])


class Measures():
    """
    For the setup and the def to read the data from the Phatlight driver

    This part of the program is a bit tricky to understand!
    Please read very cafully the comments and the docstrings.
    """
    def __init__(self):
        """
        The shield methods are only used here
        so we instanciate the object in this init.

        Also the queus for measurement are also created here.
        """
        self.adc = PiHatADC()

        # Creates an array  of queues for the measures
        # with the array index being the channel name
        # So here we have an array of 8 queues,
        # with indexes from 0 to 7
        # corresponding to the channels A0 to A7 of the ADC
        self.adc_queues = [queue.Queue()
                           for _ in range(CONFIG['I2C']['ADC_CHAN_NUM'])]

    def get_tension(self, mean, adcin):
        """
        This method is called by the others of the class
        to return the sum of a channel measuring queue.

        The result is avaraged by the mean value given
        """
        tension = self.adc.get_vol_milli_data(adcin)

        if mean == 0:
            warnings.warn("Don't devide by zero, you fool!")
            return 0

        # Put the new value in the queue
        self.adc_queues[adcin].put(tension)

        # If we have more elements than needed,
        # we pop the old ones until we have the correct nuber
        while self.adc_queues[adcin].qsize() > mean:
            self.adc_queues[adcin].get()

        # Sums all the element in the queue
        return sum(list(self.adc_queues[adcin].queue))

    def current(self, mean, adcin):
        """
        Converts the measured tension [mV] to a current [A]
        on the given adc input and average it by
        the given mean value.
        """
        return self.get_tension(mean, adcin) / (10 * mean)

    def temperature(self, mean, adcin):
        """
        Converts the measured tension [mV] to a temperature [celsius]

        To make this conversion, Phatlight provided
        a graphic on their datasheet.
        Some points were taken from this document and used in an Octave script
        to make a polynomial fit.
        This file with the datapoints is available in this repository
        in the directory "documents".

        This give us a third degree polynom:
        y = 1.9878*x^3 -13.328*x^2 + 54.034*x -27.924
        """
        temp = (self.get_tension(mean, adcin) / (1000 * mean))
        return 1.9878*(temp**3) - 13.328*(temp**2) + 54.034*temp - 27.924

    def pwm(self, mean, adcin):
        """
        Reads the output of the filter and returns
        a percent value for the PWM duty cycle.

        The tension on the output of the stm32 get lower when loaded,
        so for now the 100% is only at about 2V.

        These parameters are based on empirical tests.
        """
        return (self.get_tension(mean, adcin) / (mean))/2085 * 100

    def brightness(self, mean, adcin):
        """
        On the current test device, the max voltage produced
        by the photodiod is about 440mV.
        This will be our 100% for now.

        These parameters are based on empirical tests.
        Some more measures and calibration would help,
        especially because of the exponential
        behavior of the photodiod voltage.
        See the master-cube branch for reference.
        """
        return (self.get_tension(mean, adcin) / (mean))/411 * 100


class CSV():
    """
    Manages the methodes to read and write
    to the CSV file for data logging
    """
    def __init__(self):
        try:
            # Try to mount the Samba volume
            # using a dedecated bash script
            subprocess.call(["bash",
                             "connect_samba.sh",
                             Path(CONFIG['SMB']['MOUNT']),
                             Path(sys.argv[2]),  # samba ip
                             Path(sys.argv[5])   # samba folder name
                             ])

        except OSError as error:
            print("Errno " +
                  str(error.errno) +
                  ": could not mount the Samba volume")
            sys.exit(0)

        # First opening of the file.
        self.openf()

    def openf(self):
        """
        Dedicated to open the CSV file.
        Called in the init and every time
        the node-red want to restart measurements

        This method is separated from the __init
        becaus is needs to be called when we close
        the file and then need to reopen it.
        """
        # Setup the file descriptor for
        # the local CSV file
        my_file = Path(CONFIG['CSV']['FILE'])
        try:
            self.csvfile = open(my_file, mode="w+")
        except OSError as error:
            print("Errno " +
                  str(error.errno) +
                  ": could not load or read " +
                  str(CONFIG['CSV']['FILE']))
            sys.exit(0)

        # Add the header line to this
        # new CSV file
        self.csvfile.write("Duration from start [us], " +
                           "Current chan1 [A], " +
                           "Temperature chan1 [C], " +
                           "Current chan2 [A], " +
                           "Temperature chan2 [C], " +
                           "Brightness chan2 [%], " +
                           "Duty cycle chan2 [%], " +
                           "Duty cycle chan1 [%], " +
                           "Brightness chan1 [%]\n")

    def write(self, data):
        """
        Writes a new data to a new line in the default CSV file
        """
        self.csvfile.write(str(data) + ",\n")

    def send_to_samba(self):
        """
        Move the produced CSV file to the mounted
        Samba volume
        """
        # First we need to close the file to keep all the values
        self.csvfile.close()

        try:
            # Then we move the CSV and put the
            # UNIX epoch timestamp (in ms) as the file name
            # https://www.epochconverter.com/
            subprocess.call(["mv",
                             "-v",
                             CONFIG['CSV']['FILE'],
                             CONFIG['SMB']['MOUNT'] +
                             "/" +
                             str(round(time.time() * 1000)) +
                             "_ganzfeld" +
                             "_master" +
                             ".csv"])

        except OSError as error:
            print("Errno " +
                  str(error.errno) +
                  ": could send  " +
                  str(CONFIG['CSV']['FILE']) +
                  " to the Samba server")
            sys.exit(0)

        # These we reopen the CSV file
        # for the next set of measurement
        self.openf()


class Acquisition():
    """
    Here are the method to do the actual acquisition
    of the data.
    """
    def __init__(self):
        # This object provides the methodes to acces
        # the data of the ADC
        self.measures = Measures()

        # Object with the method to edit
        # and manage the CSV file contining
        # the measured data
        self.csv = CSV()

        # sock is the base object for the UDP
        # communication of the data
        # from this script to the Nodered
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def wait_to_go(self):
        """
        The program waits in this function
        until the value 'go' is visible
        on the Nodered http
        """
        try:
            while urllib.request.urlopen("http://" +
                                         sys.argv[1] +
                                         ":" +
                                         sys.argv[3] +
                                         "/experiment").read() != b'go':
                time.sleep(0.2)
        except IOError as error:
            print("Warning, Errno " +
                  str(error.errno) +
                  ": unable to read the NodeRed URL. Waiting...")
            time.sleep(1)
            self.wait_to_go()

    def read_loop(self):
        """
        Loops to read the ADC
        util a stop signal is received
        """

        # UNIX epoch start (in us)
        start_time = round(time.time() * 1000000)

        # In this loop we mean 100 measures of current,
        # temperature & brightness
        # and send every 7 of them to the nodered
        # We loop in it while Nodered shows "go" on its url
        while urllib.request.urlopen("http://" +
                                     sys.argv[1] +
                                     ":" +
                                     sys.argv[3] +
                                     "/experiment").read() == b'go':

            # Make all the measurements
            # Here is the ADC inputs configuration
            # for the Ganzfeld system:
            # ----------------------------------------------------------------
            # A0: max current [A] for channel 1
            # A1: temperature [celcius] for channel 1
            # A2: max current [A] for channel 2
            # A3: temperature [celcius] for channel 2
            # A4: brightness measured with external photodiod [%] for channel 2
            # A5: PWM duty cycle [%] for channel 2
            # A6: PWM duty cycle [%] for channel 1
            # A7: brightness measured with external photodiod [%] for channel 1
            # ----------------------------------------------------------------

            # Array with the list of the function we want to use
            # for the ADC inputs from A0 to A7
            adc_functions = [
                self.measures.current,
                self.measures.temperature,
                self.measures.current,
                self.measures.temperature,
                self.measures.brightness,
                self.measures.pwm,
                self.measures.pwm,
                self.measures.brightness,
            ]
            # Array with the measured values
            # initialized with zeros for each ADC entry
            adc_inputs = [0] * CONFIG['I2C']['ADC_CHAN_NUM']

            # Loop some time and only then the
            # last data is sent to the monitor.
            for _ in range(CONFIG['MONITOR_SPEED']):
                # Reads all the ADC inputs
                # and average them.
                # This acts as a low pass filter.
                for i, func in enumerate(adc_functions):
                    adc_inputs[i] = func(CONFIG['LOG_AVERAGING'], i)

                # Write the values to the CSV for the Samba volume
                # First the unix timestamg
                # then all the measured values
                # for the ADC inputs from A0 to A7
                current_time = round(time.time() * 1000000) - start_time
                self.csv.write(str(current_time) + ", " +
                               ', '.join(str(round(measure, 4))
                                         for measure in adc_inputs))

            # Creates a JSON with the measures
            # to be sent to the NodeRed instance
            monitoring = {}
            monitoring["hostname"] = str(socket.gethostname())

            monitoring["chan1"] = {}
            monitoring["chan1"]["current"] = str(round(adc_inputs[0], 3))
            monitoring["chan1"]["temperature"] = str(round(adc_inputs[1], 2))
            monitoring["chan1"]["pwm"] = str(round(adc_inputs[6], 3))
            monitoring["chan1"]["brightness"] = str(round(adc_inputs[7], 2))

            monitoring["chan2"] = {}
            monitoring["chan2"]["current"] = str(round(adc_inputs[2], 3))
            monitoring["chan2"]["temperature"] = str(round(adc_inputs[3], 2))
            monitoring["chan2"]["pwm"] = str(round(adc_inputs[5], 2))
            monitoring["chan2"]["brightness"] = str(round(adc_inputs[4], 3))

            # Prints out the whole JSON
            # Only for visual feedback
            print(json.dumps(monitoring))

            # Sending the JSON with measures
            # to the Node-Red servers
            self.sock.sendto(bytes(json.dumps(monitoring), "utf-8"),
                             (sys.argv[1],        # node red ip
                              int(sys.argv[4])))  # node red udp port

        self.csv.send_to_samba()


def main():
    """
    All the data logging and acquisition is done here
    """

    # Call the class for the main
    # two methods of this monitoring system.
    acquisition = Acquisition()

    while True:
        # Here we wait for the NodeRed signal
        # to start the measurements
        acquisition.wait_to_go()

        # Then we loop util the NodeRed
        # tells to stop
        acquisition.read_loop()


if __name__ == '__main__':
    main()
